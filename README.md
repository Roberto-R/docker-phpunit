# PHPUnit Docker Image #

This repository contains the source to a docker image running PHP 7, including some extensions and composer.  
This image is designed to run phpunit unit tests for CakePHP applications.

## Build and Run Image ##

To build the image, run:

```
docker build -t phpunit .
```

inside the repository. (Note: on some systems you might have to use `sudo ...` before every Docker command.)  
The `-t phpunit` gives the build a local tag.

After building it, you can run the image with:

```
docker run -it --name myphpunit phpunit
```

 * `phpunit` refers to the tagname. Change it if you used a different one.
 * `-it` makes the container interactive
 * `--name` gives the running container a name
