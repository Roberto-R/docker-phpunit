FROM php:7.4-cli
LABEL maintainer="robert.soor@gmai.com"

RUN apt update -q && \
# unzip: needed by composer
# libicu-dev: needed by php-intl
# libxml2-dev: needed by php-simplexml
# libpng-dev: needed by php-gd
# libzip-dev: needed by php-
    apt install -y git unzip libicu-dev libxml2-dev libpng-dev libzip-dev && \
# Get composer:
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
# Get PHP extensions:
    docker-php-ext-install intl simplexml pdo pdo_mysql gd bcmath && \
# Also get pcov for code coverage:
    pecl install pcov && docker-php-ext-enable pcov

ENTRYPOINT ["/bin/bash"]
